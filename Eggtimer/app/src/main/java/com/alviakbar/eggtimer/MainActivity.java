package com.alviakbar.eggtimer;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    TextView timeTextView;
    SeekBar seekBar;
    CountDownTimer countDownTimer;
    Boolean counterIsActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        this.timeTextView = (TextView) findViewById(R.id.timerTextView);

        seekBar.setMax(540);
        seekBar.setProgress(300);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                timeViewSetter(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                countDownTimer.cancel();
                counterIsActive = false;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void startCountDown(View view) {
        if (!counterIsActive) {

            counterIsActive = true;
            countDownTimer = new CountDownTimer(seekBar.getProgress() * 1000 + 100, 1000) {

                @Override
                public void onTick(long l) {
                    int i = (int) l / 1000;
                    timeViewSetter(i);

                }

                @Override
                public void onFinish() {
                    timeTextView.setText("0:00");
                    MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.airhorn);
                    mediaPlayer.start();
                }
            }.start();
        }
    }

    public void timeViewSetter(int secValue) {
        long minutes = TimeUnit.SECONDS.toMinutes(secValue);
        long seconds = TimeUnit.SECONDS.toSeconds(secValue) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(secValue));
        String secondStr = String.valueOf(seconds);
        if (secondStr.length() != 2) {
            secondStr = '0' + secondStr;
        }

        timeTextView.setText(String.valueOf(minutes) + ":" + secondStr);
    }

}
