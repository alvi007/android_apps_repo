package com.alviakbar.myapplication;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    public void generateTimesTable(int timesTable) {

        List<String> timesTableContent = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            timesTableContent.add(String.valueOf(timesTable * i));
        }

        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, timesTableContent));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SeekBar timesTableSeekBar = (SeekBar) findViewById(R.id.seekBar);
        timesTableSeekBar.setMax(20);
        timesTableSeekBar.setProgress(10);

        final List<Integer> myList = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listView);


        final ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, myList);
        listView.setAdapter(arrayAdapter);


        timesTableSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            /**
             * A method to set the seekbar in pogress state
             *
             * @precond seekbar should start at 1
             * @param seekBar a reference to SeekBar
             * @param i value of seekbar
             * @param b value of flag
             */
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int min = 1;
                int timesTable;

                if (i < min) {
                    timesTable = min;
                } else {
                    timesTable = i;
                }
                generateTimesTable(timesTable);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        generateTimesTable(10);

    }

}
