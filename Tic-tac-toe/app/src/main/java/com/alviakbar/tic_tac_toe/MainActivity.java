package com.alviakbar.tic_tac_toe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /*
    0 = yellow
    1 = red
    2 = unplayed
     */
    int activePlayer = 0;
    boolean gameIsActive = true;

    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2};

    int[][] winningPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void dropIn(View view) {
        ImageView counter = (ImageView) view;

        int tappedCounter = Integer.parseInt(counter.getTag().toString());

        if (gameState[tappedCounter] == 2 && gameIsActive) {

            gameState[tappedCounter] = activePlayer;

            counter.setTranslationY(-1000f);

            if (activePlayer == 0) {
                counter.setImageResource(R.drawable.yellow);
                activePlayer = 1;
            } else {
                counter.setImageResource(R.drawable.red);
                activePlayer = 0;
            }

            counter.animate().translationYBy(1000f).rotation(3500).setDuration(600);

            //check if anyone has won
            for (int[] winningPosition : winningPositions) {

                //checking if gameState for winning position
                if (gameState[winningPosition[0]] == gameState[winningPosition[1]] &&
                        gameState[winningPosition[1]] == gameState[winningPosition[2]] &&
                        gameState[winningPosition[0]] != 2) {

                    //Someone has won
                    gameIsActive = false;

                    //checking who has won
                    String winner;
                    if (gameState[winningPosition[0]] == 0) {
                        winner = "Yellow";
                    } else {
                        winner = "Red";
                    }

                    //Showing the winning Message
                    TextView winningMessage = (TextView) findViewById(R.id.winnerMessage);
                    winningMessage.setText(winner + " Won!");
                    LinearLayout layout = (LinearLayout) findViewById(R.id.playAgainLayout);
                    layout.setVisibility(View.VISIBLE);

                } else {

                    boolean gameIsOver = true;

                    //This condition is if the game turns out to be a draw
                    for (int counterState : gameState) {
                        if (counterState == 2) gameIsOver = false;
                    }

                    if (gameIsOver) {

                        /*
                        NOTE: Code Repetition !!! Need to refactoring the next following lines of code
                         */
                        //Showing the winning Message
                        TextView winningMessage = (TextView) findViewById(R.id.winnerMessage);
                        winningMessage.setText("It's a draw !");
                        LinearLayout layout = (LinearLayout) findViewById(R.id.playAgainLayout);
                        layout.setVisibility(View.VISIBLE);
                    }

                }

            }
        }

    }

    /**
     * Make the game active
     * Make the message disappear
     * set game states and players to defaults
     * clear all the src of images inside Grid Layout
     *
     * @param view
     */
    public void playAgainButtonPressed(View view) {

        gameIsActive = true;
        LinearLayout layout = (LinearLayout) findViewById(R.id.playAgainLayout);
        layout.setVisibility(View.INVISIBLE);

        activePlayer = 0;
        for (int i = 0; i < gameState.length; i++) {
            gameState[i] = 2;
        }

        GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout);

        for (int i = 0; i < gridLayout.getChildCount(); i++) {

            ((ImageView) gridLayout.getChildAt(i)).setImageResource(0);

        }

    }

}
