package com.alviakbar.videodemo2;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String path = "rtsp://rtsp2.youtube.com/CiILENy73wIaGQkDwpjrUxOWQBMYESARFEgGUgZ2aWRlb3MM/0/0/0/video.3gp";
        Uri uri = Uri.parse(path);
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        videoView.setVideoURI(uri);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);

        videoView.setMediaController(mediaController);
        videoView.requestFocus();
        videoView.start();
    }
}
