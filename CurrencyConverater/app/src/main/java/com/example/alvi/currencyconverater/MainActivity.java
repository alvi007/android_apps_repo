package com.example.alvi.currencyconverater;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    public void onClickUSDToCanada(View view){
        EditText amountConversion = (EditText) findViewById(R.id.currencyUSD);
        String USDValueString = amountConversion.getText().toString();
        double USDValue = Double.parseDouble(USDValueString);
        double CADValue = 0.746519 * USDValue;

        Toast.makeText(MainActivity.this, "CAD: " + String.format("%.2f", CADValue) ,Toast.LENGTH_SHORT).show();
        Log.i("amount", USDValueString);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
