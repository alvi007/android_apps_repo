package com.alviakbar.listviewtutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final List<String> myFriends = new ArrayList<>();
        myFriends.add("Mehdee");
        myFriends.add("Shariar");
        myFriends.add("Abid");
        myFriends.add("Fahad");

        ListView listView = (ListView) findViewById(R.id.myListView);
        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, myFriends));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), "Hello "+myFriends.get(i), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
