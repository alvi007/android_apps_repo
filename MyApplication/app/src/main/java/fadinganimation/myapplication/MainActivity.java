package fadinganimation.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    //Example of cross fading
    public void fade(View view) {
        ImageView excitedF = (ImageView) findViewById(R.id.excited_fart);
//        View danceImageView = (ImageView) findViewById(R.id.dance);

        excitedF.animate().translationYBy(1000f).setDuration(2000);
//        danceImageView.animate().alpha(1f).setDuration(2000);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
