package com.alviakbar.layouttutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ishitaButtonPressed(View view){
        Toast.makeText(MainActivity.this, "I love ishita", Toast.LENGTH_LONG).show();
    }
}
