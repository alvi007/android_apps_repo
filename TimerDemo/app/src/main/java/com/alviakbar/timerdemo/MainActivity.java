package com.alviakbar.timerdemo;

import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * One way of doing
         */
        /*
        // Handler allows chunks of code to run in a delayed way
        final Handler handler = new Handler();

        // The chunks of codes are written inside the run method in runnable
        Runnable run = new Runnable() {
            @Override
            public void run() {
                // Insert code to be run at every second


                // 1 second delay
                handler.postDelayed(this, 1000);
            }
        };

        handler.post(run);
        */

        /**
         * Second way of doing
         * useful when needed to run once as it is destroyed after being used once
         */

        new CountDownTimer(10000, 1000) {

            @Override
            public void onTick(long millisecondsUntilDone) {
                // Countdown is counting every second
                Log.i("Seconds left", String.valueOf(millisecondsUntilDone/1000));
            }

            @Override
            public void onFinish() {
                // Counter is finished! (after 10 seconds)
                Log.i("Done counting", "Success");

            }
        }.start();



    }
}
