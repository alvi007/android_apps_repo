package com.alviakbar.myvideodemo2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.dont_let_me_down);
    TextView songStatusView = (TextView) findViewById(R.id.songStatusView);
    //Boolean songStarted;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.songStarted = false;
    }

    public void playButtonPressed(View view) {
        songStatusView.setText("Status: Playing");
        this.mediaPlayer.start();

    }

    public void pauseButtonPressed(View view) {
        songStatusView.setText("Status: Paused");
        this.mediaPlayer.pause();
    }

    public void stopButtonPressed(View view) {
        songStatusView.setText("Status: Stopped");
        this.mediaPlayer.stop();

    }
}
