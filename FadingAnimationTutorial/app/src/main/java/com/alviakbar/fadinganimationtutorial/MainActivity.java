package com.alviakbar.fadinganimationtutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        ImageView myImageView = (ImageView) findViewById(R.id.myImage);
        myImageView.setTranslationX(-2000f);
        */
    }

    public void fade(View view) {
        ImageView myImageView = (ImageView) findViewById(R.id.myImage);

//        ImageView myImageView2 = (ImageView) findViewById(R.id.myImage2);
//        myImageView2.animate().alpha(0f).setDuration(2000);

        myImageView.animate().scaleXBy(0.5f).scaleYBy(0.5f).setDuration(1000);

    }
}
