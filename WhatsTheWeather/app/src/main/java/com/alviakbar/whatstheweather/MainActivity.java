package com.alviakbar.whatstheweather;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    private static final String APPID = "8c2e3293dfb74d319f06d30f53971850";
    private String cityName = "Saskatoon";
    private String apiUrl = "http://api.openweathermap.org/data/2.5/weather?q="+cityName+"&APPID="+APPID;

    private TextView cityNameView;
    private TextView countryNameView;
    private TextView latitudeAndLongitudeView;
    private TextView sunriseTextView;
    private TextView sunsetTextView;
    private TextView temperatureCelciusTextView;
    private TextView weatherMainTypeView;
    private TextView weatherDescriptionView;
    private TextView humidityView;

    public class DownloadWeatherContent extends AsyncTask<String, Void, String > {

        URL url;
        HttpURLConnection urlConnection = null;

        @Override
        protected String doInBackground(String... urls) {
            try {

                String result = "";
                int data;

                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                while((data= reader.read()) >= 0){
                    result += (char) data;
                }

                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // need to do the UI stuff here ! background task does not work with UI !!!
            displayWeather(result);


        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DownloadWeatherContent downloadWeatherContentTask = new DownloadWeatherContent();
        downloadWeatherContentTask.execute(apiUrl);

        this.cityNameView = (TextView) findViewById(R.id.cityTextViewId);
        this.countryNameView = (TextView) findViewById(R.id.countryTextViewId);
        this.latitudeAndLongitudeView = (TextView) findViewById(R.id.latitudeAndLongitudeViewId);
        this.sunriseTextView = (TextView) findViewById(R.id.sunriseTextView);
        this.sunsetTextView = (TextView) findViewById(R.id.sunsetTextView);
        this.temperatureCelciusTextView = (TextView) findViewById(R.id.temperatureTextView);
        this.weatherMainTypeView = (TextView) findViewById(R.id.weatherType);
        this.weatherDescriptionView = (TextView) findViewById(R.id.weatherDescription);
        this.humidityView = (TextView) findViewById(R.id.humidityView);
    }

    public void displayWeather(String result) {

        try {
            // getting city name
            JSONObject jsonObject = new JSONObject(result);
            String cityName = jsonObject.getString("name");

            // getting country related information
            JSONObject sysInfo = jsonObject.getJSONObject("sys");
            String countryName = sysInfo.getString("country");

            // getting coordinates
            JSONObject coordinatesInfo = jsonObject.getJSONObject("coord");
            String latitude = coordinatesInfo.getString("lat");
            String longitude = coordinatesInfo.getString("lon");

            //getting sunrise and sunset hours and correctly formatting it into a time format
            Date sunriseTimeDate = new Date(sysInfo.getLong("sunrise")*1000L); // convert seconds into milliseconds
            Date sunsetTimeDate = new Date(sysInfo.getLong("sunset")*1000L); // convert seconds into milliseconds

            DateFormat formatter = new SimpleDateFormat("hh:mm a");
            formatter.setTimeZone(TimeZone.getTimeZone("GMT-6"));

            String sunriseTimeFormatted = formatter.format(sunriseTimeDate);
            String sunsetTimeFormatted = formatter.format(sunsetTimeDate);

            // Getting current temperature in centigrade
            JSONObject mainInfo = jsonObject.getJSONObject("main");
            String currentTemperature = String.valueOf((int)(mainInfo.getDouble("temp") - 273.15));

            // Getting weather type and weather description
            JSONArray weatherInfoArray = jsonObject.getJSONArray("weather");
            JSONObject weatherInfo = (JSONObject) weatherInfoArray.get(0);
            String weatherInfoMain = weatherInfo.getString("main");
            String weatherDescription = weatherInfo.getString("description");

            // Getting weather humidity
            String humidityInfo = jsonObject.getJSONObject("main").getJSONObject("humidity").toString();


            // setting display
            this.cityNameView.setText(cityName);
            this.countryNameView.setText(countryName);
            this.latitudeAndLongitudeView.setText(String.format("Latitude/Longitude: %s/%s", latitude, longitude));
            this.sunriseTextView.setText("Sunrise: "+sunriseTimeFormatted);
            this.sunsetTextView.setText("Sunset: "+sunsetTimeFormatted);
            this.temperatureCelciusTextView.setText(String.format("%s\u00b0C", currentTemperature));
            this.weatherMainTypeView.setText(weatherInfoMain);
            this.weatherDescriptionView.setText(weatherDescription);
            //this.humidityView.setText();


        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("API Content", result);


    }
}
