package com.alviakbar.downloadingwebcontent;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    /**
     * the first string takes the weblink where the content needs to be downloaded
     * the second void, does stuff while the task is running eg. showing the progress of download using a progress bar
     * the third field return the downloaded content
     */
    public class DownloadTask extends AsyncTask<String, Void, String > {

        @Override
        protected String doInBackground(String... urls) {

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;


            try {

                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = urlConnection.getInputStream();

                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();

                while (data != -1) {
                    char current = (char) data;
                    result += current;
                }

                return result;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return "Failed";
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        android.os.Debug.waitForDebugger();

        String result = null;
        DownloadTask task = new DownloadTask();
        try {

            result = task.execute("https://www.google.ca/").get();

        } catch (InterruptedException e) {

            e.printStackTrace();

        } catch (ExecutionException e) {

            e.printStackTrace();

        }

        Log.i("Content of URL", result);
    }
}
