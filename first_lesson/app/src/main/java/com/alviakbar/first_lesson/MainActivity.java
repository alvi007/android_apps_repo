package com.alviakbar.first_lesson;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // If there is a saved instance within the user's phone, it will restore that
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changeImage(View view) {
        ImageView gigImageView = (ImageView) findViewById(R.id.gigImageView);
        TextView quoteTextView = (TextView) findViewById(R.id.quoteTextView);

        gigImageView.setImageResource(R.drawable.trump_kiss);
        quoteTextView.setText("I am so going to destroy the world");

    }

}
